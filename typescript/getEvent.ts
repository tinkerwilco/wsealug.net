import { parse, Component } from "ical.js"

class Event {
  when: Date;
  where: string;
  what: string;
  description: string;
  url: string;

  constructor(when: Date, where: string, what: string, description: string, url: string) {
    this.when = when;
    this.where = where;
    this.what = what;
    this.description = description;
    this.url = url;
  }

}

// generate (accumulate) a string of HTML for a single event .
function generateEventHtml(ins: string, ev : Event) : string {
  const options : Intl.DateTimeFormatOptions = { timeZone: 'America/Los_Angeles',  
                                                 weekday:  'long',
                                                 month:    'long', 
                                                 day:      'numeric', 
                                                 hour:     'numeric', 
                                                 minute:   'numeric',
                                               };

  const when = ev.when.toLocaleString('en-US', options);
                     ins += "<br>";
                     ins += "<h5 >" + ev.what + "</h5>";
                     ins += "<a href=\"" + ev.url + "\" target=\"_blank\">";
                     ins +=   "<ul>";
                     ins +=     "<li>" + when        + "</li>";
                     ins +=     "<li>" + ev.where       + "</li>";
  if(ev.description) ins +=     "<li>" + ev.description + "</li>";
                     ins +=     "<li>" + ev.url         + "</li>";
                     ins +=   "</ul>";
                     ins += "</a>";
  return ins;
}

// Get an ics calendar from url. 
// convert  to html and pass it on to callback.
async function getEvent(icsUrl : string, setHtmlCallbackback : (html:string) => any) {
  try {
    const response = await fetch(icsUrl);
    const event = await response.text();
    const jcalData = parse(event);
    const vcalendar = new Component(jcalData);
    const vevents = vcalendar.getAllSubcomponents('vevent');
    const evs = vevents.map(ev => { const dstart = ev.getFirstPropertyValue('dtstart');
                                    return new Event( new Date(Date.parse(dstart)), 
                                                      ev.getFirstPropertyValue('location'),
                                                      ev.getFirstPropertyValue('summary'),
                                                      ev.getFirstPropertyValue('description'),
                                                      ev.getFirstPropertyValue('url') ); })
                       .sort((a,b) => a.when - b.when);

    const ins = evs.reduce(generateEventHtml, "<h4>Upcoming Events:</h4>");

    setHtmlCallbackback (ins);
  } catch (err) {
    setHtmlCallbackback(err);
  }
}

// per: https://stackoverflow.com/questions/23296094/browserify-how-to-call-function-bundled-in-a-file-generated-through-browserify
// and: https://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript

(<any>window).getEvent = getEvent;
